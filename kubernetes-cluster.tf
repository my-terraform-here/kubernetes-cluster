locals {
  folder_id   = "b1ggdjoi04jt2on955er"
}

resource "yandex_kubernetes_cluster" "k8s-game" {
  name = "k8s-game"
  network_id = yandex_vpc_network.mynet.id
  master {
    public_ip = true
    version = "1.28"
    zonal {
      zone      = "ru-central1-d"
      subnet_id = yandex_vpc_subnet.mysubnet.id
    }
    security_group_ids = [yandex_vpc_security_group.k8s-public-services.id]
  }
  service_account_id      = yandex_iam_service_account.myaccount.id
  node_service_account_id = yandex_iam_service_account.myaccount.id
  depends_on = [
    yandex_resourcemanager_folder_iam_member.admin
    #ПРИМЕРЫ ДОБАВЛЕНИЯ ОТДЕЛЬНЫХ ЗАВИСИМОСТЕЙ
    #yandex_resourcemanager_folder_iam_member.k8s-clusters-agent,
    #yandex_resourcemanager_folder_iam_member.vpc-public-admin,
    #yandex_resourcemanager_folder_iam_member.images-puller,
    #yandex_resourcemanager_folder_iam_member.encrypterDecrypter,
    #yandex_resourcemanager_folder_iam_member.dns-admin,
    #yandex_resourcemanager_folder_iam_member.balancer-admin
  ]
  kms_provider {
    key_id = yandex_kms_symmetric_key.kms-key.id
  }
}
resource "yandex_dns_zone" "devops-zone" {
  name        = "devops-zone"
  description = "externalzone"
  zone        = "devops.com."
  public      = true
  #ПРИМЕР. сеть для private DNS zone
  #private_networks = ["enpnbd6bbakcs18oq0tq"]
}
resource "yandex_kubernetes_node_group" "k8s-game" {
  cluster_id  = yandex_kubernetes_cluster.k8s-game.id
  name        = "node-group-0"
  version     = "1.28"

  instance_template {
    platform_id = "standard-v2"
    network_interface {
      ipv4 = true
      nat = true
      subnet_ids = [yandex_vpc_subnet.mysubnet.id]
      security_group_ids = [yandex_vpc_security_group.k8s-public-services.id]
      #ПРИМЕР. Добавить DNS запись для группы узлов.
      #ipv4_dns_records {
      #  fqdn        = "test"
      #  dns_zone_id = yandex_dns_zone.devops-zone.id
      #}
    }

    resources {
      memory = 2
      cores  = 2
      core_fraction = 5
    }

    boot_disk {
      type = "network-hdd"
      size = 64
    }

    scheduling_policy {
      preemptible = false
    }

    container_runtime {
      type = "containerd"
    }
  }

  scale_policy {
    fixed_scale {
      size = 2
    }
  }

  allocation_policy {
    location {
      zone = "ru-central1-d"
    }
  }

  maintenance_policy {
    auto_upgrade = false
    auto_repair  = true
  }
}

resource "yandex_vpc_network" "mynet" {
  name = "k8s-terraform-net"
}

resource "yandex_vpc_subnet" "mysubnet" {
  name = "k8s-terraform-subnet"
  v4_cidr_blocks = ["10.1.0.0/16"]
  zone           = "ru-central1-d"
  network_id     = yandex_vpc_network.mynet.id
}

resource "yandex_iam_service_account" "myaccount" {
  name        = "k8s-terraform-svc-account"
  description = "K8S zonal service account"
}

resource "yandex_resourcemanager_folder_iam_member" "admin" {
  # Сервисному аккаунту назначается роль "admin".
  folder_id = local.folder_id
  role      = "admin"
  member    = "serviceAccount:${yandex_iam_service_account.myaccount.id}"
}
#######ПРИМЕРЫ ДОБАВЛЕНИЯ ОТДЕЛЬНЫХ РОЛЕЙ
#resource "yandex_resourcemanager_folder_iam_member" "k8s-clusters-agent" {
#  # Сервисному аккаунту назначается роль "k8s.clusters.agent".
#  folder_id = local.folder_id
#  role      = "k8s.clusters.agent"
#  member    = "serviceAccount:${yandex_iam_service_account.myaccount.id}"
#}
#
#resource "yandex_resourcemanager_folder_iam_member" "vpc-public-admin" {
#  # Сервисному аккаунту назначается роль "vpc.publicAdmin".
#  folder_id = local.folder_id
#  role      = "vpc.publicAdmin"
#  member    = "serviceAccount:${yandex_iam_service_account.myaccount.id}"
#}
#
#resource "yandex_resourcemanager_folder_iam_member" "images-puller" {
#  # Сервисному аккаунту назначается роль "container-registry.images.puller".
#  folder_id = local.folder_id
#  role      = "container-registry.images.puller"
#  member    = "serviceAccount:${yandex_iam_service_account.myaccount.id}"
#}
#
#resource "yandex_resourcemanager_folder_iam_member" "encrypterDecrypter" {
#  # Сервисному аккаунту назначается роль "kms.keys.encrypterDecrypter".
#  folder_id = local.folder_id
#  role      = "kms.keys.encrypterDecrypter"
#  member    = "serviceAccount:${yandex_iam_service_account.myaccount.id}"
#}
#
#resource "yandex_resourcemanager_folder_iam_member" "dns-admin" {
#  # Сервисному аккаунту назначается роль "dns-admin".
#  folder_id = local.folder_id
#  role      = "dns.admin"
#  member    = "serviceAccount:${yandex_iam_service_account.myaccount.id}"
#}
#
#resource "yandex_resourcemanager_folder_iam_member" "balancer-admin" {
#  # Сервисному аккаунту назначается роль "dns-admin".
#  folder_id = local.folder_id
#  role      = "load-balancer.admin"
#  member    = "serviceAccount:${yandex_iam_service_account.myaccount.id}"
#}
#
resource "yandex_kms_symmetric_key" "kms-key" {
  # Ключ Yandex Key Management Service для шифрования важной информации, такой как пароли, OAuth-токены и SSH-ключи.
  name              = "kms-key"
  default_algorithm = "AES_128"
  rotation_period   = "8760h" # 1 год.
}


resource "yandex_vpc_security_group" "k8s-public-services" {
  name        = "k8s-terraform-group"
  description = "Правила группы разрешают подключение к сервисам из интернета. Примените правила только для групп узлов."
  network_id  = yandex_vpc_network.mynet.id
  ingress {
    protocol          = "ANY"
    description       = "Правило ingress"
    v4_cidr_blocks    = ["0.0.0.0/0"]
  }
  egress {
    protocol          = "ANY"
    description       = "Правило egress"
    v4_cidr_blocks    = ["0.0.0.0/0"]
  }
  #ПРИМЕРЫ использования политик INGRESS/EGRESS
  #ingress {
  #  protocol          = "ANY"
  #  description       = "Правило разрешает взаимодействие мастер-узел и узел-узел внутри группы безопасности."
  #  predefined_target = "self_security_group"
  #  from_port         = 0
  #  to_port           = 65535
  #}
  #ingress {
  #  protocol          = "ANY"
  #  description       = "Правило разрешает взаимодействие под-под и сервис-сервис. Укажите подсети вашего кластера Managed Service for Kubernetes и сервисов."
  #  v4_cidr_blocks    = concat(yandex_vpc_subnet.mysubnet.v4_cidr_blocks)
  #  from_port         = 0
  #  to_port           = 65535
  #}
  #ingress {
  #  protocol          = "TCP"
  #  description       = "Правило разрешает входящий трафик из интернета на диапазон портов NodePort. Добавьте или измените порты на нужные вам."
  #  v4_cidr_blocks    = ["0.0.0.0/0"]
  #  from_port         = 30000
  #  to_port           = 32767
  #}
  #egress {
  #  protocol          = "ANY"
  #  description       = "Правило разрешает весь исходящий трафик. Узлы могут связаться с Yandex Container Registry, Yandex Object Storage, Docker Hub и т. д."
  #  v4_cidr_blocks    = ["0.0.0.0/0"]
  #  from_port         = 0
  #  to_port           = 65535
  #}
  
  
}
